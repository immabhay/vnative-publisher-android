# vNative Publisher

Publishers working with vNative performance marketing software networks can take business to go with the vNative Publisher Mobile App. Your Campaign performance is always at your fingertips, no matter where you are.

Key features:
- Measure Performance Real-Time
- Easy charts showing payout, conversions, clicks and impressions
- Better Comparisons between time frames to quickly measure performance changes

**Playstore link : [https://play.google.com/store/apps/details?id=com.vnative.vnativepublisher](https://play.google.com/store/apps/details?id=com.vnative.vnativepublisher)**

## Screenshots

<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/1.jpg" width="200" style="padding:10px;"/>
<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/2.png" width="200" style="padding:10px;"/>
<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/3.png" width="200" style="padding:10px;"/>
<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/4.jpg" width="200" style="padding:10px;"/>
<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/5.jpg" width="200" style="padding:10px;"/>
<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/6.jpg" width="200" style="padding:10px;"/>
<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/7.jpg" width="200" style="padding:10px;"/>
<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/8.jpg" width="200" style="padding:10px;"/>
<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/9.jpg" width="200" style="padding:10px;"/>
<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/10.png" width="200" style="padding:10px;"/>
<img src="http://images.abhaychauhan.xyz/screenshots/android/vnative/publisher/11.png" width="200" style="padding:10px;"/>

## Libraries used
- Picasso [Docs](http://square.github.io/picasso/2.x/picasso/)
- hellocharts-android [Library](https://github.com/lecho/hellocharts-android)

## Devices tested on
| Device     | Android Version   | Skin/ROM |
|------------|:------------------|----------|
| Lenovo A369i | 4.4.2| Lenovo ROM |
| Redmi Note 3| 6.0|MIUI|
| Nexus 5| 7.0| Stock Android|


## Maintainers
The project is maintained by
- Abhay Chauhan ([@immabhay](https://github.com/immabhay))
