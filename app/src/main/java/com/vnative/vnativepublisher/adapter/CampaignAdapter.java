package com.vnative.vnativepublisher.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vnative.vnativepublisher.R;
import com.vnative.vnativepublisher.model.Campaign;
import com.vnative.vnativepublisher.utils.UrlUtils;

import java.util.ArrayList;

public class CampaignAdapter extends RecyclerView.Adapter<CampaignAdapter.CampaignViewHolder> {

    private ArrayList<Campaign> campaigns;
    private Context context;
    private CampaignClickListener campaignClickListener;

    public interface CampaignClickListener {
        void onCampaignClick(View view, int position);
    }

    public CampaignAdapter(Context context, ArrayList<Campaign> campaigns,
                           CampaignClickListener campaignClickListener) {
        this.context = context;
        this.campaigns = campaigns;
        this.campaignClickListener = campaignClickListener;
    }

    public class CampaignViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        public ImageView campaignIv;
        public TextView titleTv, rateTv, categoryTv;

        public CampaignViewHolder(View view) {
            super(view);
            campaignIv = (ImageView) view.findViewById(R.id.campaign_image);
            titleTv = (TextView) view.findViewById(R.id.campaign_title);
            rateTv = (TextView) view.findViewById(R.id.campaign_rate);
            categoryTv = (TextView) view.findViewById(R.id.campaign_category);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            campaignClickListener.onCampaignClick(v, position);
        }
    }

    @Override
    public CampaignViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.campaign_card,
                parent, false);
        return new CampaignViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CampaignViewHolder holder, int position) {
        Campaign campaign = campaigns.get(position);

        Picasso.with(getContext())
                .load(UrlUtils.VNATIVE_IMAGE_BASE_URL + campaign.getImage())
                .placeholder(R.drawable.placeholder_image)
                .error(R.drawable.placeholder_image)
                .into(holder.campaignIv);

        holder.titleTv.setText(campaign.getTitle());
        holder.rateTv.setText(campaign.getRate());

        String categoryText = "";
        for (int index = 0; index < campaign.getCategory().size(); index++) {
            categoryText += campaign.getCategory().get(index);
            if (index != campaign.getCategory().size() - 1) {
                categoryText += ", ";
            }
        }

        holder.categoryTv.setText(categoryText);

    }

    @Override
    public int getItemCount() {
        return campaigns.size();
    }

    private Context getContext() {
        return this.context;
    }

}
