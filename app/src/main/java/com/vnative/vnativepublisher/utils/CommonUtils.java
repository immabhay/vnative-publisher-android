package com.vnative.vnativepublisher.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.text.format.Formatter;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

public class CommonUtils {

    public static String COLOR_CLICKS = "#1E88E5";
    public static String COLOR_CONVERSIONS = "#FFB300";
    public static String COLOR_IMPRESSIONS = "#EF6C00";
    public static String COLOR_REVENUE = "#009688";

    /**
     * This function returns true if json contains the data, else if return false
     *
     * @param response
     * @return
     */
    public static boolean hasData(JSONObject response) {
        boolean isDataFound = false;
        try {
            isDataFound = response.getBoolean("success");
        } catch (JSONException e) {
            isDataFound = false;
        }
        return isDataFound;
    }

    public static String convertPrice(Double revenue) {
        DecimalFormat format = new DecimalFormat("0.00");
        return format.format(revenue);
    }

    /**
     * This function return true if device is connected to internet and network, else it returns
     * false.
     *
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }
        return false;
    }

    /**
     * This function return date of the present day.
     *
     * @return
     */
    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * This function return the date of last one week from the current date.
     *
     * @param currentDate
     * @return
     * @throws ParseException
     */
    public static String getLastWeekDate(String currentDate) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(simpleDateFormat.parse(currentDate));
        cal.add(Calendar.DATE, -7);
        return simpleDateFormat.format(cal.getTime());
    }

    /**
     * This function return the date of the next one week from the current date.
     *
     * @param currentDate
     * @return
     * @throws ParseException
     */
    public static String getNextWeekDate(String currentDate) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(simpleDateFormat.parse(currentDate));
        cal.add(Calendar.DATE, 7);
        return simpleDateFormat.format(cal.getTime());
    }

    /**
     * This function returns date from date time string
     *
     * @param stringDate
     * @return
     */
    public static String getDate(String stringDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return format.format(date);
    }

    /**
     * This function add all the integer elements of list.
     *
     * @param arrayList
     * @return
     */
    public static int sumIntegerListElements(ArrayList<Integer> arrayList) {
        int sum = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            sum += arrayList.get(i);
        }
        return sum;
    }

    /**
     * This function add all float elements of list.
     *
     * @param arrayList
     * @return
     */
    public static Double sumDoubleListElements(ArrayList<Float> arrayList) {
        Double sum = 0.0;
        for (int i = 0; i < arrayList.size(); i++) {
            sum += arrayList.get(i);
        }
        return sum;
    }

    public static String getDateString(String dateString) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(simpleDateFormat.parse(dateString));
        return simpleDateFormat.format(cal.getTime());
    }

    public static String getTimeString(String time) throws ParseException {
        DateFormat timeFormat = new SimpleDateFormat("hh:mm a");
        try {
            Date date = timeFormat.parse(time);
            return date.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return time;
        }
    }

    public static String getDateTimeString(String createdString) {
        String[] dateTimeString = createdString.split(" ");
        String date = null;
        String time = null;
        try {
            date = getDateString(dateTimeString[0]);
//            time = getTimeString(dateTimeString[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * This function checks status and return status as Active or Inactive in string format
     *
     * @param status
     * @return
     */
    public static String getStatus(Boolean status) {
        if (status) {
            return "Active";
        }
        return "Inactive";
    }

    /**
     * This function return the max element value from arraylist
     *
     * @param list
     * @return
     */
    public static int getMax(ArrayList<Integer> list) {
        int max = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > max) {
                max = list.get(i);
            }
        }
        return max;
    }

    /**
     * Return the device id
     *
     * @param context
     * @return
     */
    public static String getDeviceId(Context context) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    /**
     * Return the local ip address of the mobile device
     *
     * @return
     */
    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("Main", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("MAIN", ex.toString());
        }
        return null;
    }

    /**
     * Return the mac address of the device
     *
     * @return
     */
    public static String getMacAddress() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

}
