package com.vnative.vnativepublisher.utils;


public class UrlUtils {

    public static String AFFILATE_PAYOUT_URL = "https://api.vnative.com/affiliate/payout";
    public static String AFFILATE_CAMPAIGN_URL = "https://api.vnative.com/affiliate/campaigns/";
    public static String VNATIVE_IMAGE_BASE_URL = "http://cdn.vnative.com/images/";
    public static String AFFILATE_ACCOUNT_URL = "https://api.vnative.com/affiliate/account/";
    public static String AFFILATE_REPORT_URL = "https://api.vnative.com/affiliate/report";
    public static String AFFILATE_REQUEST_ACCESS = "https://api.vnative.com/affiliate/requestAccess/";
    public static String AFFILATE_LINKS_URL = "https://api.vnative.com/affiliate/link/";
    public static String PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=com.vnative.vnativepublisher";
    public static String AFFILATE_LOGIN_ACCESS = "https://api.vnative.com/affiliate/loginAccess";
    public static String PUBLIC_IP_URL = "http://wtfismyip.com/text";
}
