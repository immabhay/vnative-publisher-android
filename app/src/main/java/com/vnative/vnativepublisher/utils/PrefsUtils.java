package com.vnative.vnativepublisher.utils;


import android.content.Context;
import android.content.SharedPreferences;

public class PrefsUtils {

    /**
     * This function set logged in preference as true.
     *
     * @param context
     * @return
     */
    public static void setLoggedInAsTrue(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("loggedin", true);
        editor.commit();
    }

    /**
     * This function set logged in preference as false
     *
     * @param context
     */
    public static void setLoggedInAsFalse(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("loggedin", false);
        editor.commit();
    }

    /**
     * This function returns logged in status.
     *
     * @param context
     * @return
     */
    public static boolean isLoggedIn(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        return prefs.getBoolean("loggedin", false);
    }

    /**
     * This function store user api key.
     *
     * @param context
     * @param userApiKey
     */
    public static void setUserApiKey(Context context, String userApiKey) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("vnativeapikey", userApiKey);
        editor.commit();
    }

    /**
     * This function retrieve api key from shared preference.
     *
     * @param context
     * @return
     */
    public static String getUserApiKey(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        return prefs.getString("vnativeapikey", "null");
    }

    /**
     * This function return true if publisher details is stored in preference, else return false
     *
     * @param context
     * @return
     */
    public static Boolean isPublisherDetailStored(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        return prefs.getBoolean("publisherdetail", false);
    }

    /**
     * This function set publisher detail store as true
     *
     * @param context
     */
    public static void setPublisherDetailStoredAsTrue(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("publisherdetail", true);
        editor.commit();
    }

    /**
     * This function set publisher detail store as false
     *
     * @param context
     */
    public static void setPublisherDetailStoredAsFalse(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("publisherdetail", false);
        editor.commit();
    }


    /**
     * This function store publisher name
     *
     * @param context
     * @param publisherName
     */
    public static void setPublisherName(Context context, String publisherName) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("publishername", publisherName);
        editor.commit();
    }

    /**
     * This function return publisher name
     *
     * @param context
     * @return
     */
    public static String getPublisherName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        return prefs.getString("publishername", null);
    }

    /**
     * This function store the name of the publisher logo
     *
     * @param context
     * @param publisherImage
     */
    public static void setPublisherLogo(Context context, String publisherImage) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("publisherlogo", publisherImage);
        editor.commit();
    }

    /**
     * This function return publisher logo name
     *
     * @param context
     * @return
     */
    public static String getPublisherLogo(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("vnativeSharedPref",
                Context.MODE_PRIVATE);
        return prefs.getString("publisherlogo", null);
    }

}
