package com.vnative.vnativepublisher.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.vnative.vnativepublisher.R;
import com.vnative.vnativepublisher.utils.PrefsUtils;

public class SplashActivity extends AppCompatActivity {

    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 3600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        hideActionBar();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PrefsUtils.isLoggedIn(SplashActivity.this)) {
                    startActivity(new Intent(SplashActivity.this, AffilateActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }

            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    public void hideActionBar() {
        try {
            getSupportActionBar().hide();
            getActionBar().hide();
        } catch (Exception e) {//ignore}
        }
    }
}
