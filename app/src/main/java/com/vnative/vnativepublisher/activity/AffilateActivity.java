package com.vnative.vnativepublisher.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vnative.vnativepublisher.R;
import com.vnative.vnativepublisher.fragment.AccountFragment;
import com.vnative.vnativepublisher.fragment.CampaignFragment;
import com.vnative.vnativepublisher.fragment.DashboardFragment;
import com.vnative.vnativepublisher.interfaces.PublisherDetail;
import com.vnative.vnativepublisher.utils.CommonUtils;
import com.vnative.vnativepublisher.utils.PrefsUtils;
import com.vnative.vnativepublisher.utils.UrlUtils;


public class AffilateActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PublisherDetail {

    private NavigationView navigationView;
    private TextView publisherNameTextView;
    private ImageView publisherLogoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affilate);

        if (!CommonUtils.isConnected(this)) {
            finish();
            startActivity(new Intent(this, NetworkActivity.class));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        publisherNameTextView = (TextView) headerView.findViewById(R.id.publisher_name_tv);
        publisherLogoView = (ImageView) headerView.findViewById(R.id.publisher_logo_iv);

        if (PrefsUtils.isPublisherDetailStored(this) && PrefsUtils.getPublisherLogo(this) != null) {
            updatePublisherDetail();
        }

        // Display dashboard by default when the activity loads for the first time
        displaySelectedFragment(R.id.nav_dashboard);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        displaySelectedFragment(id);
        return true;
    }

    private void displaySelectedFragment(int itemId) {
        Fragment fragment = null;
        switch (itemId) {
            case R.id.nav_dashboard:
                fragment = new DashboardFragment();
                break;
            case R.id.nav_campaign:
                fragment = new CampaignFragment();
                break;
            case R.id.nav_account:
                fragment = new AccountFragment();
                break;
            case R.id.nav_feedback:
                openFeedbackIntent();
                break;
            case R.id.nav_logout:
                PrefsUtils.setUserApiKey(AffilateActivity.this, "null");
                PrefsUtils.setLoggedInAsFalse(AffilateActivity.this);
                PrefsUtils.setPublisherDetailStoredAsFalse(AffilateActivity.this);
                PrefsUtils.setPublisherName(AffilateActivity.this, null);
                PrefsUtils.setPublisherLogo(AffilateActivity.this, null);
                finish();
                startActivity(new Intent(AffilateActivity.this, LoginActivity.class));
                break;
        }

        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void updatePublisherDetail() {
        String name = PrefsUtils.getPublisherName(this);
        String logo = PrefsUtils.getPublisherLogo(this);
        if (name != "" || name != null) {
            publisherNameTextView.setText(name);
        }

        if (logo != "" || logo != null) {
            Picasso.with(this).load(UrlUtils.VNATIVE_IMAGE_BASE_URL +
                    logo).into(publisherLogoView);
        }
    }

    private void openFeedbackIntent() {
        Uri feedbackPage = Uri.parse(UrlUtils.PLAY_STORE_LINK);
        Intent intent = new Intent(Intent.ACTION_VIEW, feedbackPage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}