package com.vnative.vnativepublisher.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vnative.vnativepublisher.R;
import com.vnative.vnativepublisher.utils.CommonUtils;
import com.vnative.vnativepublisher.utils.HttpsTrustManager;
import com.vnative.vnativepublisher.utils.NetworkController;
import com.vnative.vnativepublisher.utils.PrefsUtils;
import com.vnative.vnativepublisher.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import github.nisrulz.qreader.QRDataListener;
import github.nisrulz.qreader.QREader;

import static android.Manifest.permission.CAMERA;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout qrLoginLayout, apiLoginLayout;
    private SurfaceView surfaceView;
    private Button loginViaApiBtn, loginViaQRBtn, submitBtn;
    private EditText apiKeyInputText;
    private QREader qrEader;
    private TextView apiKeyText;
    private String publicIpAddress;

    private static final int PERMISSION_REQUEST_CODE = 200;

    public boolean isValid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        hideActionBar();
        isValid = false;
        IpTask ipTask = new IpTask();
        ipTask.execute();

        surfaceView = (SurfaceView) findViewById(R.id.camera_view);
        qrLoginLayout = (RelativeLayout) findViewById(R.id.qr_login_view);
        apiLoginLayout = (RelativeLayout) findViewById(R.id.api_login_view);
        loginViaApiBtn = (Button) findViewById(R.id.login_via_api_key);
        loginViaQRBtn = (Button) findViewById(R.id.login_via_qr_btn);
        submitBtn = (Button) findViewById(R.id.submit_btn);
        apiKeyInputText = (EditText) findViewById(R.id.api_key_input);
        apiKeyText = (TextView) findViewById(R.id.api_key);

        loginViaQRBtn.setOnClickListener(this);
        loginViaApiBtn.setOnClickListener(this);
        submitBtn.setOnClickListener(this);

        if (!checkPermission()) {
            requestPermission();
        }

        qrEader = new QREader.Builder(this, surfaceView, new QRDataListener() {
            @Override
            public void onDetected(final String data) {
                Log.d("QREader", "Value : " + data);
                apiKeyText.post(new Runnable() {
                    @Override
                    public void run() {
                        apiKeyText.setText(data);
                        checkApiKey(getApplicationContext(), data);
                        if (isValid) {
                            PrefsUtils.setUserApiKey(LoginActivity.this, data);
                            PrefsUtils.setLoggedInAsTrue(LoginActivity.this);
                            sendLoginReport(LoginActivity.this);
                            finish();
                            startActivity(new Intent(LoginActivity.this, AffilateActivity.class));
                        } else {
                        }
                    }
                });


            }
        }).facing(QREader.BACK_CAM)
                .enableAutofocus(true)
                .height(surfaceView.getHeight())
                .width(surfaceView.getWidth())
                .build();


    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (!cameraAccepted) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LoginActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onClick(View view) {
        if (view == findViewById(R.id.login_via_api_key)) {
            qrLoginLayout.setVisibility(View.GONE);
            apiLoginLayout.setVisibility(View.VISIBLE);
        } else if (view == findViewById(R.id.login_via_qr_btn)) {
            apiLoginLayout.setVisibility(View.GONE);
            qrLoginLayout.setVisibility(View.VISIBLE);
        } else if (view == findViewById(R.id.submit_btn)) {
            String apiKey = apiKeyInputText.getText().toString();
            if (apiKey.trim().isEmpty()) {
                Toast.makeText(this, "Api key cannot be empty !!", Toast.LENGTH_SHORT).show();
            }
            checkApiKey(getApplicationContext(), apiKey.trim());
            if (isValid) {
                PrefsUtils.setUserApiKey(LoginActivity.this, apiKey.trim());
                PrefsUtils.setLoggedInAsTrue(LoginActivity.this);

                // Toast.makeText(this, "Key" + apiKey, Toast.LENGTH_SHORT).show();
                finish();
                startActivity(new Intent(LoginActivity.this, AffilateActivity.class));

            } else {
                // Toast.makeText(this, "Invalid login !!", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void hideActionBar() {
        try {
            getSupportActionBar().hide();
            getActionBar().hide();
        } catch (Exception e) {//ignore}
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        qrEader.initAndStart(surfaceView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrEader.releaseAndCleanup();
    }

    private void checkApiKey(Context context, final String apiKey) {

        RequestQueue queue = Volley.newRequestQueue(context);
        HttpsTrustManager.allowAllSSL();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, UrlUtils.AFFILATE_PAYOUT_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        setIsValid(CommonUtils.hasData(response));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        setIsValid(false);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerSys = super.getHeaders();
                Map<String, String> headers = new HashMap<>();
                headers.putAll(headerSys);
                headers.put("X-Api-Key", apiKey);
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };
        queue.add(jsonObjectRequest);
    }

    private void setIsValid(boolean valid) {
        isValid = valid;
    }

    private void sendLoginReport(final Context context) {
        JSONObject base = new JSONObject();

        JSONObject data = new JSONObject();
        try {
            data.put("ip", publicIpAddress);
            data.put("mac_addr", CommonUtils.getMacAddress());
            data.put("device_id", CommonUtils.getDeviceId(context));
            base.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpsTrustManager.allowAllSSL();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.AFFILATE_LOGIN_ACCESS, base, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (CommonUtils.hasData(response)) {
                    showSnackbarMessage("Login Successfull");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showSnackbarMessage("Not able to authenticate successfully!!!");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerSys = super.getHeaders();
                Map<String, String> headers = new HashMap<>();
                headers.putAll(headerSys);
                headers.put("X-Api-Key", PrefsUtils.getUserApiKey(context));
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };
        NetworkController.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    private void showSnackbarMessage(String message) {
        Snackbar.make(qrLoginLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    private String getPublicIpAddress() {
        URL url = null;
        try {
            url = new URL(UrlUtils.PUBLIC_IP_URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }

        String jsonResponse = "";
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

        return jsonResponse.trim();
    }

    /**
     * Function take InputStream object, read data from stream , convert to String and return the String.
     *
     * @param inputStream
     * @return output (String)
     * @throws IOException
     */
    public static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    public class IpTask extends AsyncTask<Void, Void, String>{
        @Override
        protected String doInBackground(Void... params) {
            return getPublicIpAddress();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            publicIpAddress = s;
        }
    }
}
