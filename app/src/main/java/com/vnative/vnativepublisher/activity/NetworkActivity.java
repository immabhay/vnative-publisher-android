package com.vnative.vnativepublisher.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.vnative.vnativepublisher.R;
import com.vnative.vnativepublisher.utils.CommonUtils;

public class NetworkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);
    }

    public void retryNetwork(View view) {
        if (CommonUtils.isConnected(this)) {
            finish();
            startActivity(new Intent(this, AffilateActivity.class));
        }
    }

}
