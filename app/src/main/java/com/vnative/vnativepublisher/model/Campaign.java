package com.vnative.vnativepublisher.model;

import java.util.ArrayList;

public class Campaign {

    private String _id;
    private String title;
    private ArrayList<String> category;
    private String image;
    private String rate;

    public Campaign(String _id, String title, ArrayList<String> category, String image, String rate) {
        this._id = _id;
        this.title = title;
        this.category = category;
        this.image = image;
        this.rate = rate;
    }

    /**
     * Return the Id of campaign
     *
     * @return
     */
    public String getId() {
        return this._id;
    }

    /**
     * Return the title of campaign
     *
     * @return
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Return a list of category
     *
     * @return
     */
    public ArrayList<String> getCategory() {
        return this.category;
    }

    /**
     * Return the name of image
     *
     * @return
     */
    public String getImage() {
        return this.image;
    }

    /**
     * Return the rate of campaign commission
     *
     * @return
     */
    public String getRate() {
        return this.rate;
    }

}
