package com.vnative.vnativepublisher.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.squareup.picasso.Picasso;
import com.vnative.vnativepublisher.R;
import com.vnative.vnativepublisher.interfaces.PublisherDetail;
import com.vnative.vnativepublisher.utils.CommonUtils;
import com.vnative.vnativepublisher.utils.HttpsTrustManager;
import com.vnative.vnativepublisher.utils.PrefsUtils;
import com.vnative.vnativepublisher.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;


public class DashboardFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private PublisherDetail publisherDetail;

    private TextView volumeReportTv;
    private TextView clickTextView;
    private TextView impressionTextView;
    private TextView conversionTextView;
    private TextView earningTextView;
    private TextView dateRangeTextView;
    private TextView clicksDataTextView;
    private TextView conversionsDataTextView;
    private TextView impressionsDataTextView;
    private TextView earningsDataTextView;

    private LinearLayout volumeReportLayout;
    private LinearLayout multiDateRangeLayout;

    private ProgressBar pbVolumeReport;
    private ProgressBar pbClicksGraph;
    private ProgressBar pbConversionsGraph;
    private ProgressBar pbImpressionsGraph;
    private ProgressBar pbEarningsGraph;

    ArrayList<String> date;
    ArrayList<Integer> clicksData;
    ArrayList<Integer> impressionsData;
    ArrayList<Integer> conversionsData;
    ArrayList<Float> earningsData;

    private String startDate;
    private String endDate;

    LineChartView clicksGraph;
    LineChartView impressionsGraph;
    LineChartView conversionsGraph;
    LineChartView earningsGraph;

    private SwipeRefreshLayout swipeRefreshLayout;

    private ImageView backDateRangeBtn;
    private ImageView forwardDateRangeBtn;

    public DashboardFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);

        //Initialising Horizontal Layout TextView
        clickTextView = (TextView) rootView.findViewById(R.id.tv_clicks);
        impressionTextView = (TextView) rootView.findViewById(R.id.tv_impressions);
        conversionTextView = (TextView) rootView.findViewById(R.id.tv_conversions);
        earningTextView = (TextView) rootView.findViewById(R.id.tv_earnings);
        dateRangeTextView = (TextView) rootView.findViewById(R.id.date_range_tv);

        // Graphs Data TextView initialize
        clicksDataTextView = (TextView) rootView.findViewById(R.id.tv_clicks_data);
        impressionsDataTextView = (TextView) rootView.findViewById(R.id.tv_impressions_data);
        conversionsDataTextView = (TextView) rootView.findViewById(R.id.tv_conversions_data);
        earningsDataTextView = (TextView) rootView.findViewById(R.id.tv_revenue_data);

        volumeReportTv = (TextView) rootView.findViewById(R.id.tv_volume_report);

        // GraphView Initialize
        clicksGraph = (LineChartView) rootView.findViewById(R.id.graph_clicks);
        impressionsGraph = (LineChartView) rootView.findViewById(R.id.graph_impressions);
        conversionsGraph = (LineChartView) rootView.findViewById(R.id.graph_conversions);
        earningsGraph = (LineChartView) rootView.findViewById(R.id.graph_earnings);

        // Volume Report layout initialize
        volumeReportLayout = (LinearLayout) rootView.findViewById(R.id.layout_volume_report);

        multiDateRangeLayout = (LinearLayout) rootView.findViewById(R.id.multi_date_range_layout);
        multiDateRangeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        DashboardFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
//                dpd.setAutoHighlight(mAutoHighlight);
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        // Progress Bar initialize
        pbClicksGraph = (ProgressBar) rootView.findViewById(R.id.pb_clicks_graph);
        pbConversionsGraph = (ProgressBar) rootView.findViewById(R.id.pb_conversions_graph);
        pbImpressionsGraph = (ProgressBar) rootView.findViewById(R.id.pb_impressions_graph);
        pbEarningsGraph = (ProgressBar) rootView.findViewById(R.id.pb_revenue_graph);
        pbVolumeReport = (ProgressBar) rootView.findViewById(R.id.pb_volume_report);

        backDateRangeBtn = (ImageView) rootView.findViewById(R.id.btn_backward);
        forwardDateRangeBtn = (ImageView) rootView.findViewById(R.id.btn_forward);

        backDateRangeBtn.setOnClickListener(this);
        forwardDateRangeBtn.setOnClickListener(this);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setOnRefreshListener(DashboardFragment.this);

        // ArrayList Initialize
        date = new ArrayList<>();
        clicksData = new ArrayList<>();
        impressionsData = new ArrayList<>();
        conversionsData = new ArrayList<>();
        earningsData = new ArrayList<>();

        try {
            endDate = CommonUtils.getCurrentDate();
            startDate = CommonUtils.getLastWeekDate(endDate);
        } catch (ParseException e) {
            startDate = "2016-10-01";
        }
        setDateRangeView(startDate, endDate);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        requestData(startDate, endDate);
                                    }
                                }
        );
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String publisherName = PrefsUtils.getPublisherName(getContext());
        if (publisherName != null) {
            getActivity().setTitle(publisherName);
        } else {
            getActivity().setTitle(getString(R.string.app_name));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        startDate = year + "-" + (++monthOfYear) + "-" + dayOfMonth;
        endDate = yearEnd + "-" + (++monthOfYearEnd) + "-" + dayOfMonthEnd;
        setDateRangeView(startDate, endDate);
        requestData(startDate, endDate);
    }

    @Override
    public void onRefresh() {
        requestData(startDate, endDate);
    }

    @Override
    public void onClick(View view) {
        int clickedBtnId = view.getId();
        switch (clickedBtnId) {
            case R.id.btn_backward:
                dateRangeBackward();
                break;
            case R.id.btn_forward:
                dateRangeForward();
                break;
        }
    }

    public void displayData(JSONObject response) {
        // Clearing stored data of clicks, impressions, conversions, revenue, dates
        clearData();

        JSONObject dataObject = response.optJSONObject("data");
        JSONObject statsObject = dataObject.optJSONObject("stats");
        Iterator<String> keys = (Iterator<String>) statsObject.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            try {
                if (statsObject.get(key) instanceof JSONObject) {
                    JSONObject dateObject = new JSONObject(statsObject.get(key).toString());
                    int clicks = dateObject.getInt("clicks");
                    int impressions = dateObject.getInt("impressions");
                    int conversions = dateObject.getInt("conversions");
                    float payout = (float) dateObject.getDouble("payout");
                    date.add(key);
                    clicksData.add(clicks);
                    impressionsData.add(impressions);
                    conversionsData.add(conversions);
                    earningsData.add(payout);
                }
            } catch (JSONException e) {
            }
        }
        // Clicks Graph
        displayClickGraph();
        // Impressions Graph
        displayImpressionsGraph();
        // Conversions Graph
        displayConversionsGraph();
        // Earnings Graph
        displayEarningsGraph();

        JSONObject totalObject = dataObject.optJSONObject("total");
        int click = totalObject.optInt("clicks");
        int impressions = totalObject.optInt("impressions");
        int conversions = totalObject.optInt("conversions");
        Double payout = totalObject.optDouble("payout");

        showVolumeReport();

        if (click > 0) {
            showClicksData();
            clickTextView.setText(Integer.toString(click));
        } else {
            showClicksError();
            clickTextView.setText(Integer.toString(0));
        }

        if (impressions > 0) {
            showImpressionsData();
            impressionTextView.setText(Integer.toString(impressions));
        } else {
            showImpressionsError();
            impressionTextView.setText(Integer.toString(0));
        }

        if (conversions > 0) {
            showConversionsData();
            conversionTextView.setText(Integer.toString(conversions));
        } else {
            showConversionsError();
            conversionTextView.setText(Integer.toString(0));
        }

        if (!payout.isNaN() && payout > 0.0) {
            showEarningsData();
            earningTextView.setText("$" + CommonUtils.convertPrice(payout));
        } else {
            showEarningsError();
            earningTextView.setText("$0.0");
        }

        swipeRefreshLayout.setRefreshing(false);
    }

    private void requestData(String startDate, String endDate) {
        startLoading();
        RequestQueue queue = Volley.newRequestQueue(getContext());
        HttpsTrustManager.allowAllSSL();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, UrlUtils.AFFILATE_PAYOUT_URL
                + "?start=" + startDate + "&end=" + endDate, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (CommonUtils.hasData(response)) {
                            displayData(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerSys = super.getHeaders();
                Map<String, String> headers = new HashMap<>();
                headers.putAll(headerSys);
                headers.put("X-Api-Key", PrefsUtils.getUserApiKey(getContext()));
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };

        queue.add(jsonObjectRequest);

        JsonObjectRequest logoRequest = new JsonObjectRequest(Request.Method.GET,
                UrlUtils.AFFILATE_ACCOUNT_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (CommonUtils.hasData(response)) {
                            storePublisherData(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Error updating application !!!", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerSys = super.getHeaders();
                Map<String, String> headers = new HashMap<>();
                headers.putAll(headerSys);
                headers.put("X-Api-Key", PrefsUtils.getUserApiKey(getContext()));
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };
        if (!PrefsUtils.isPublisherDetailStored(getContext()) && PrefsUtils.getPublisherLogo(getContext()) == null) {
            queue.add(logoRequest);
        }
    }

    /**
     * This function store publisher name and logo detail in preferences
     *
     * @param response
     */
    private void storePublisherData(JSONObject response) {
        if (!response.has("data")) {
            PrefsUtils.setPublisherName(getContext(), "null");
            return;
        }
        JSONObject data = response.optJSONObject("data");
        if (!data.has("network")) {
            PrefsUtils.setPublisherName(getContext(), "null");
            return;
        }
        JSONObject network = data.optJSONObject("network");
        String publisherName = network.optString("name");
        PrefsUtils.setPublisherName(getContext(), publisherName);
        PrefsUtils.setPublisherDetailStoredAsTrue(getContext());

        JSONObject appearance = network.optJSONObject("appearance");
        if (!appearance.has("logo")) {
            PrefsUtils.setPublisherLogo(getContext(), null);
            return;
        }
        String publisherLogo = appearance.optString("logo");
        PrefsUtils.setPublisherLogo(getContext(), publisherLogo);
        updatePublisherDashboard();
    }

    private void updatePublisherDashboard() {
        String publisherName = PrefsUtils.getPublisherName(getContext());
        if (publisherName != null ||
                !publisherName.isEmpty()) {
            getActivity().setTitle(publisherName);
        } else {
            getActivity().setTitle(getString(R.string.app_name));
        }

        TextView publisherNameTv = (TextView) getActivity().findViewById(R.id.publisher_name_tv);
        publisherNameTv.setText(publisherName);

        ImageView publisherLogoView = (ImageView) getActivity().findViewById(R.id.publisher_logo_iv);
        String publisherLogo = PrefsUtils.getPublisherLogo(getContext());
        if (!publisherLogo.isEmpty() || publisherLogo != null) {
            Picasso.with(getContext()).load(UrlUtils.VNATIVE_IMAGE_BASE_URL +
                    publisherLogo).into(publisherLogoView);
        }
        Snackbar snackbar = Snackbar.make(swipeRefreshLayout,
                "Application updated", Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    /**
     * This function will display the clicks graph
     */
    private void displayClickGraph() {
        List<AxisValue> axisValues = new ArrayList<>();
        List<PointValue> values = new ArrayList<>();
        for (int i = 0; i < date.size(); i++) {
            values.add(new PointValue(i, clicksData.get(i)));
            axisValues.add(new AxisValue(i).setLabel(date.get(i)));
        }
        Line line = new Line(values);
        line.setHasPoints(false);
        line.setColor(Color.parseColor(CommonUtils.COLOR_CLICKS));
        List<Line> lines = new ArrayList<>();
        lines.add(line);
        LineChartData clicksGraphData = new LineChartData(lines);
        clicksGraphData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
        clicksGraphData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));
        clicksGraph.setLineChartData(clicksGraphData);
        clicksGraph.setZoomType(ZoomType.HORIZONTAL);
    }

    /**
     * This function will display the impressions graph
     */
    private void displayImpressionsGraph() {
        List<AxisValue> axisValues = new ArrayList<>();
        List<PointValue> values = new ArrayList<>();
        for (int i = 0; i < date.size(); i++) {
            values.add(new PointValue(i, impressionsData.get(i)));
            axisValues.add(new AxisValue(i).setLabel(date.get(i)));
        }
        Line line = new Line(values);
        line.setHasPoints(false);
        line.setColor(Color.parseColor(CommonUtils.COLOR_IMPRESSIONS));
        List<Line> lines = new ArrayList<>();
        lines.add(line);
        LineChartData impressionsGraphData = new LineChartData(lines);
        impressionsGraphData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
        impressionsGraphData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));
        impressionsGraph.setLineChartData(impressionsGraphData);
        impressionsGraph.setZoomType(ZoomType.HORIZONTAL);
    }

    /**
     * This function will display the converions graph
     */
    private void displayConversionsGraph() {
        List<AxisValue> axisValues = new ArrayList<>();
        List<PointValue> values = new ArrayList<>();
        for (int i = 0; i < date.size(); i++) {
            values.add(new PointValue(i, conversionsData.get(i)));
            axisValues.add(new AxisValue(i).setLabel(date.get(i)));
        }
        Line line = new Line(values);
        line.setHasPoints(false);
        line.setColor(Color.parseColor(CommonUtils.COLOR_CONVERSIONS));
        List<Line> lines = new ArrayList<>();
        lines.add(line);
        LineChartData conversionsGraphData = new LineChartData(lines);
        conversionsGraphData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
        conversionsGraphData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));
        conversionsGraph.setLineChartData(conversionsGraphData);
        conversionsGraph.setZoomType(ZoomType.HORIZONTAL);
    }

    /**
     * This function will display the earnings data
     */
    private void displayEarningsGraph() {
        List<AxisValue> axisValues = new ArrayList<>();
        List<PointValue> values = new ArrayList<>();
        for (int i = 0; i < date.size(); i++) {
            values.add(new PointValue(i, earningsData.get(i)));
            axisValues.add(new AxisValue(i).setLabel(date.get(i)));
        }
        Line line = new Line(values);
        line.setHasPoints(false);

        line.setColor(Color.parseColor(CommonUtils.COLOR_REVENUE));
        List<Line> lines = new ArrayList<>();
        lines.add(line);
        LineChartData earningsGraphData = new LineChartData(lines);
        earningsGraphData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
        earningsGraphData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));
        earningsGraph.setLineChartData(earningsGraphData);
        earningsGraph.setZoomType(ZoomType.HORIZONTAL);
    }

    /**
     * This function change the date range to the last wee from the current week.
     */
    public void dateRangeBackward() {
        endDate = startDate;
        try {
            startDate = CommonUtils.getLastWeekDate(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        requestData(startDate, endDate);
        setDateRangeView(startDate, endDate);
    }

    /**
     * This function change the date range to next one week from the current week.
     */
    public void dateRangeForward() {
        startDate = endDate;
        try {
            endDate = CommonUtils.getNextWeekDate(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        requestData(startDate, endDate);
        setDateRangeView(startDate, endDate);
    }

    /**
     * This function display the date in the date range text view.
     *
     * @param startDate
     * @param endDate
     */
    private void setDateRangeView(String startDate, String endDate) {
        dateRangeTextView.setText(startDate + " - " + endDate);
    }

    /**
     * This function clears all the data stored in the lists
     */
    private void clearData() {
        date.clear();
        clicksData.clear();
        conversionsData.clear();
        impressionsData.clear();
        earningsData.clear();
    }

    private void showClicksError() {
        clicksGraph.setVisibility(View.INVISIBLE);
        pbClicksGraph.setVisibility(View.INVISIBLE);
        clicksDataTextView.setVisibility(View.VISIBLE);
    }

    private void showClicksData() {
        pbClicksGraph.setVisibility(View.INVISIBLE);
        clicksDataTextView.setVisibility(View.INVISIBLE);
        clicksGraph.setVisibility(View.VISIBLE);
    }

    private void showClicksLoading() {
        clicksDataTextView.setVisibility(View.INVISIBLE);
        clicksGraph.setVisibility(View.INVISIBLE);
        pbClicksGraph.setVisibility(View.VISIBLE);
    }

    private void showConversionsError() {
        conversionsGraph.setVisibility(View.INVISIBLE);
        pbConversionsGraph.setVisibility(View.INVISIBLE);
        conversionsDataTextView.setVisibility(View.VISIBLE);
    }

    private void showConversionsData() {
        pbConversionsGraph.setVisibility(View.INVISIBLE);
        conversionsDataTextView.setVisibility(View.INVISIBLE);
        conversionsGraph.setVisibility(View.VISIBLE);
    }

    private void showConversionsLoading() {
        conversionsDataTextView.setVisibility(View.INVISIBLE);
        conversionsGraph.setVisibility(View.INVISIBLE);
        pbConversionsGraph.setVisibility(View.VISIBLE);
    }

    private void showImpressionsError() {
        impressionsGraph.setVisibility(View.INVISIBLE);
        pbImpressionsGraph.setVisibility(View.INVISIBLE);
        impressionsDataTextView.setVisibility(View.VISIBLE);
    }

    private void showImpressionsData() {
        pbImpressionsGraph.setVisibility(View.INVISIBLE);
        impressionsDataTextView.setVisibility(View.INVISIBLE);
        impressionsGraph.setVisibility(View.VISIBLE);
    }

    private void showImpressionsLoading() {
        impressionsDataTextView.setVisibility(View.INVISIBLE);
        impressionsGraph.setVisibility(View.INVISIBLE);
        pbImpressionsGraph.setVisibility(View.VISIBLE);
    }

    private void showEarningsError() {
        pbEarningsGraph.setVisibility(View.INVISIBLE);
        earningsGraph.setVisibility(View.INVISIBLE);
        earningsDataTextView.setVisibility(View.VISIBLE);
    }

    private void showEarningsData() {
        earningsDataTextView.setVisibility(View.INVISIBLE);
        pbEarningsGraph.setVisibility(View.INVISIBLE);
        earningsGraph.setVisibility(View.VISIBLE);
    }

    private void showEarningsLoading() {
        earningsDataTextView.setVisibility(View.INVISIBLE);
        earningsGraph.setVisibility(View.INVISIBLE);
        pbEarningsGraph.setVisibility(View.VISIBLE);
    }

    private void hideVolumeReport() {
        volumeReportLayout.setVisibility(View.INVISIBLE);
        pbVolumeReport.setVisibility(View.VISIBLE);
    }

    private void showVolumeReport() {
        pbVolumeReport.setVisibility(View.INVISIBLE);
        volumeReportLayout.setVisibility(View.VISIBLE);
    }

    private void startLoading() {
        hideVolumeReport();
        showClicksLoading();
        showConversionsLoading();
        showImpressionsLoading();
        showEarningsLoading();
    }

}
