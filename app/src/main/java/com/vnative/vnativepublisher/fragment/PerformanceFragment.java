package com.vnative.vnativepublisher.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.vnative.vnativepublisher.R;
import com.vnative.vnativepublisher.utils.CommonUtils;
import com.vnative.vnativepublisher.utils.HttpsTrustManager;
import com.vnative.vnativepublisher.utils.PrefsUtils;
import com.vnative.vnativepublisher.utils.UrlUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PerformanceFragment extends Fragment implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener  {

    private TextView dateRangeTextView;
    private TextView clicksDataTextView;
    private TextView conversionsDataTextView;
    private TextView impressionsDataTextView;
    private TextView earningsDataTextView;

    private ProgressBar pbClicksGraph;
    private ProgressBar pbConversionsGraph;
    private ProgressBar pbImpressionsGraph;
    private ProgressBar pbEarningsGraph;

    private ArrayList<String> date;
    private ArrayList<Integer> clicksData;
    private ArrayList<Integer> impressionsData;
    private ArrayList<Integer> conversionsData;
    private ArrayList<Float> earningsData;

    private String startDate;
    private String endDate;

    private LineChartView clicksGraph;
    private LineChartData clicksGraphData;

    private LineChartView impressionsGraph;
    private LineChartData impressionsGraphData;

    private LineChartView conversionsGraph;
    private LineChartData conversionsGraphData;

    private LineChartView earningsGraph;
    private LineChartData earningsGraphData;

    private ImageView forwardImageView, backwardImageView;

    private LinearLayout multiDateRangeLayout;

    public PerformanceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_performance, container, false);

        dateRangeTextView = (TextView) rootView.findViewById(R.id.date_range_tv);
        // Graphs Data TextView initialize
        clicksDataTextView = (TextView) rootView.findViewById(R.id.tv_clicks_data);
        impressionsDataTextView = (TextView) rootView.findViewById(R.id.tv_impressions_data);
        conversionsDataTextView = (TextView) rootView.findViewById(R.id.tv_conversions_data);
        earningsDataTextView = (TextView) rootView.findViewById(R.id.tv_revenue_data);

        // GraphView Initialize
        clicksGraph = (LineChartView) rootView.findViewById(R.id.graph_clicks);
        impressionsGraph = (LineChartView) rootView.findViewById(R.id.graph_impressions);
        conversionsGraph = (LineChartView) rootView.findViewById(R.id.graph_conversions);
        earningsGraph = (LineChartView) rootView.findViewById(R.id.graph_earnings);

        // Progress Bar initialize
        pbClicksGraph = (ProgressBar) rootView.findViewById(R.id.pb_clicks_graph);
        pbConversionsGraph = (ProgressBar) rootView.findViewById(R.id.pb_conversions_graph);
        pbImpressionsGraph = (ProgressBar) rootView.findViewById(R.id.pb_impressions_graph);
        pbEarningsGraph = (ProgressBar) rootView.findViewById(R.id.pb_revenue_graph);

        multiDateRangeLayout = (LinearLayout) rootView.findViewById(
                R.id.multi_date_range_performance_layout);
        multiDateRangeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        PerformanceFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
//                dpd.setAutoHighlight(mAutoHighlight);
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        // ArrayList Initialize
        date = new ArrayList<>();
        clicksData = new ArrayList<>();
        impressionsData = new ArrayList<>();
        conversionsData = new ArrayList<>();
        earningsData = new ArrayList<>();

        // ImageView initialize
        backwardImageView = (ImageView) rootView.findViewById(R.id.go_backward_iv);
        forwardImageView = (ImageView) rootView.findViewById(R.id.go_forward_iv);

        backwardImageView.setOnClickListener(this);
        forwardImageView.setOnClickListener(this);

        try {
            endDate = CommonUtils.getCurrentDate();
            startDate = CommonUtils.getLastWeekDate(endDate);
        } catch (ParseException e) {
            startDate = "2016-10-01";
        }

        setDateRangeView(startDate, endDate);
        requestData(startDate, endDate);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("Datepickerdialog");
        if(dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        startDate = year + "-" + (++monthOfYear) + "-" + dayOfMonth;
        endDate = yearEnd + "-" + (++monthOfYearEnd) + "-" + dayOfMonthEnd;
        setDateRangeView(startDate, endDate);
        requestData(startDate, endDate);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.go_backward_iv:
                goBackwardOneWeek();
                break;
            case R.id.go_forward_iv:
                goForwardOneWeek();
                break;
        }
    }

    private void requestData(String startDate, String endDate) {
        startLoading();
        RequestQueue queue = Volley.newRequestQueue(getContext());
        HttpsTrustManager.allowAllSSL();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, UrlUtils.AFFILATE_REPORT_URL
                + "?start=" + startDate + "&end=" + endDate, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (CommonUtils.hasData(response)) {
                            displayData(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerSys = super.getHeaders();
                Map<String, String> headers = new HashMap<>();
                headers.putAll(headerSys);
                headers.put("X-Api-Key", PrefsUtils.getUserApiKey(getContext()));
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };
        queue.add(jsonObjectRequest);
    }

    public void displayData(JSONObject response) {
        // Clearing stored data of clicks, impressions, conversions, revenue, dates
        clearData();

        JSONObject dataObject = response.optJSONObject("data");
        JSONArray performanceArray = dataObject.optJSONArray("performance");
        for (int i = 0; i < performanceArray.length(); i++) {
            JSONObject performanceChildObject = performanceArray.optJSONObject(i);
            Integer clicks = performanceChildObject.optInt("clicks");
            Integer conversions = performanceChildObject.optInt("conversions");
            Integer impressions = performanceChildObject.optInt("impressions");
            Double revenue = performanceChildObject.optDouble("revenue");
            String created = CommonUtils.getDate(performanceChildObject.optString("created"));
            date.add(created);

            if (clicks == null) {
                clicks = 0;
                clicksData.add(clicks);
            } else {
                clicksData.add(clicks);
            }

            if (conversions == null) {
                conversions = 0;
                conversionsData.add(conversions);
            } else {
                conversionsData.add(conversions);
            }

            if (impressions == null) {
                impressions = 0;
                impressionsData.add(impressions);
            } else {
                impressionsData.add(impressions);
            }

            if (revenue.isNaN() || revenue == null) {
                revenue = 0.0;
                earningsData.add(revenue.floatValue());
            } else {
                earningsData.add(revenue.floatValue());
            }
        }

        // Clicks Graph
        displayClickGraph();
        // Impressions Graph
        displayImpressionsGraph();
        // Conversions Graph
        displayConversionsGraph();
        // Earnings Graph
        displayEarningsGraph();

        if (CommonUtils.sumIntegerListElements(clicksData) > 0) {
            showClicksData();
        } else {
            showClicksError();
        }

        if (CommonUtils.sumIntegerListElements(impressionsData) > 0) {
            showImpressionsData();
        } else {
            showImpressionsError();
        }

        if (CommonUtils.sumIntegerListElements(conversionsData) > 0) {
            showConversionsData();
        } else {
            showConversionsError();
        }

        if (CommonUtils.sumDoubleListElements(earningsData) > 0.0) {
            showEarningsData();
        } else {
            showEarningsError();
        }

    }

    /**
     * This function will display the clicks graph
     */
    private void displayClickGraph() {
        List<AxisValue> axisValues = new ArrayList<>();
        List<PointValue> values = new ArrayList<>();
        for (int i = 0; i < date.size(); i++) {
            values.add(new PointValue(i, clicksData.get(i)));
            axisValues.add(new AxisValue(i).setLabel(date.get(i)));
        }
        Line line = new Line(values);
        line.setHasPoints(false);
        line.setColor(Color.parseColor(CommonUtils.COLOR_CLICKS));
        List<Line> lines = new ArrayList<>();
        lines.add(line);
        clicksGraphData = new LineChartData(lines);
        clicksGraphData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
        clicksGraphData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));
        clicksGraph.setLineChartData(clicksGraphData);
        clicksGraph.setZoomType(ZoomType.HORIZONTAL);
    }

    /**
     * This function will display the impressions graph
     */
    private void displayImpressionsGraph() {
        List<AxisValue> axisValues = new ArrayList<>();
        List<PointValue> values = new ArrayList<>();
        for (int i = 0; i < date.size(); i++) {
            values.add(new PointValue(i, impressionsData.get(i)));
            axisValues.add(new AxisValue(i).setLabel(date.get(i)));
        }
        Line line = new Line(values);
        line.setHasPoints(false);
        line.setColor(Color.parseColor(CommonUtils.COLOR_IMPRESSIONS));
        List<Line> lines = new ArrayList<>();
        lines.add(line);
        impressionsGraphData = new LineChartData(lines);
        impressionsGraphData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
        impressionsGraphData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));
        impressionsGraph.setLineChartData(impressionsGraphData);
        impressionsGraph.setZoomType(ZoomType.HORIZONTAL);
    }

    /**
     * This function will display the converions graph
     */
    private void displayConversionsGraph() {
        List<AxisValue> axisValues = new ArrayList<>();
        List<PointValue> values = new ArrayList<>();
        for (int i = 0; i < date.size(); i++) {
            values.add(new PointValue(i, conversionsData.get(i)));
            axisValues.add(new AxisValue(i).setLabel(date.get(i)));
        }
        Line line = new Line(values);
        line.setHasPoints(false);
        line.setColor(Color.parseColor(CommonUtils.COLOR_CONVERSIONS));
        List<Line> lines = new ArrayList<>();
        lines.add(line);
        conversionsGraphData = new LineChartData(lines);
        conversionsGraphData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
        conversionsGraphData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));
        conversionsGraph.setLineChartData(conversionsGraphData);
        conversionsGraph.setZoomType(ZoomType.HORIZONTAL);
    }

    /**
     * This function will display the earnings data
     */
    private void displayEarningsGraph() {
        List<AxisValue> axisValues = new ArrayList<>();
        List<PointValue> values = new ArrayList<>();
        for (int i = 0; i < date.size(); i++) {
            values.add(new PointValue(i, earningsData.get(i)));
            axisValues.add(new AxisValue(i).setLabel(date.get(i)));
        }
        Line line = new Line(values);
        line.setHasPoints(false);
        line.setColor(Color.parseColor(CommonUtils.COLOR_REVENUE));
        List<Line> lines = new ArrayList<>();
        lines.add(line);
        earningsGraphData = new LineChartData(lines);
        earningsGraphData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
        earningsGraphData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));
        earningsGraph.setLineChartData(earningsGraphData);
        earningsGraph.setZoomType(ZoomType.HORIZONTAL);
    }

    private void setDateRangeView(String startDate, String endDate) {
        dateRangeTextView.setText(startDate + " - " + endDate);
    }

    public void goBackwardOneWeek() {
        endDate = startDate;
        try {
            startDate = CommonUtils.getLastWeekDate(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        requestData(startDate, endDate);
        setDateRangeView(startDate, endDate);
    }

    public void goForwardOneWeek() {
        startDate = endDate;
        try {
            endDate = CommonUtils.getNextWeekDate(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        requestData(startDate, endDate);
        setDateRangeView(startDate, endDate);
    }

    private void clearData() {
        date.clear();
        clicksData.clear();
        conversionsData.clear();
        impressionsData.clear();
        earningsData.clear();
    }

    private void showClicksError() {
        clicksGraph.setVisibility(View.INVISIBLE);
        pbClicksGraph.setVisibility(View.INVISIBLE);
        clicksDataTextView.setVisibility(View.VISIBLE);
    }

    private void showClicksData() {
        pbClicksGraph.setVisibility(View.INVISIBLE);
        clicksDataTextView.setVisibility(View.INVISIBLE);
        clicksGraph.setVisibility(View.VISIBLE);
    }

    private void showClicksLoading() {
        clicksDataTextView.setVisibility(View.INVISIBLE);
        clicksGraph.setVisibility(View.INVISIBLE);
        pbClicksGraph.setVisibility(View.VISIBLE);
    }

    private void showConversionsError() {
        conversionsGraph.setVisibility(View.INVISIBLE);
        pbConversionsGraph.setVisibility(View.INVISIBLE);
        conversionsDataTextView.setVisibility(View.VISIBLE);
    }

    private void showConversionsData() {
        pbConversionsGraph.setVisibility(View.INVISIBLE);
        conversionsDataTextView.setVisibility(View.INVISIBLE);
        conversionsGraph.setVisibility(View.VISIBLE);
    }

    private void showConversionsLoading() {
        conversionsDataTextView.setVisibility(View.INVISIBLE);
        conversionsGraph.setVisibility(View.INVISIBLE);
        pbConversionsGraph.setVisibility(View.VISIBLE);
    }

    private void showImpressionsError() {
        impressionsGraph.setVisibility(View.INVISIBLE);
        pbImpressionsGraph.setVisibility(View.INVISIBLE);
        impressionsDataTextView.setVisibility(View.VISIBLE);
    }

    private void showImpressionsData() {
        pbImpressionsGraph.setVisibility(View.INVISIBLE);
        impressionsDataTextView.setVisibility(View.INVISIBLE);
        impressionsGraph.setVisibility(View.VISIBLE);
    }

    private void showImpressionsLoading() {
        impressionsDataTextView.setVisibility(View.INVISIBLE);
        impressionsGraph.setVisibility(View.INVISIBLE);
        pbImpressionsGraph.setVisibility(View.VISIBLE);
    }

    private void showEarningsError() {
        pbEarningsGraph.setVisibility(View.INVISIBLE);
        earningsGraph.setVisibility(View.INVISIBLE);
        earningsDataTextView.setVisibility(View.VISIBLE);
    }

    private void showEarningsData() {
        earningsDataTextView.setVisibility(View.INVISIBLE);
        pbEarningsGraph.setVisibility(View.INVISIBLE);
        earningsGraph.setVisibility(View.VISIBLE);
    }

    private void showEarningsLoading() {
        earningsDataTextView.setVisibility(View.INVISIBLE);
        earningsGraph.setVisibility(View.INVISIBLE);
        pbEarningsGraph.setVisibility(View.VISIBLE);
    }

    private void startLoading() {
        showClicksLoading();
        showConversionsLoading();
        showImpressionsLoading();
        showEarningsLoading();
    }

}
