package com.vnative.vnativepublisher.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vnative.vnativepublisher.R;
import com.vnative.vnativepublisher.activity.CampaignDetailActivity;
import com.vnative.vnativepublisher.adapter.CampaignAdapter;
import com.vnative.vnativepublisher.model.Campaign;
import com.vnative.vnativepublisher.utils.CommonUtils;
import com.vnative.vnativepublisher.utils.HttpsTrustManager;
import com.vnative.vnativepublisher.utils.PrefsUtils;
import com.vnative.vnativepublisher.utils.UrlUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CampaignFragment extends Fragment implements CampaignAdapter.CampaignClickListener {

    private RecyclerView campaignRv;
    private ArrayList<Campaign> campaigns;
    private CampaignAdapter adapter;
    private LinearLayout loadingScreenLayout;
    private LinearLayout noDataLayout;
    private LinearLayoutManager layoutManager;

    public CampaignFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_campaign, container, false);

        layoutManager = new LinearLayoutManager(getContext());

        loadingScreenLayout = (LinearLayout) rootView.findViewById(R.id.loading_screen);
        noDataLayout = (LinearLayout) rootView.findViewById(R.id.no_data);
        campaignRv = (RecyclerView) rootView.findViewById(R.id.campaign_rv);
        campaigns = new ArrayList<>();
        adapter = new CampaignAdapter(getContext(), campaigns, this);

        campaignRv.setLayoutManager(layoutManager);
        campaignRv.setAdapter(adapter);

        RequestQueue queue = Volley.newRequestQueue(getContext());
        HttpsTrustManager.allowAllSSL();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                UrlUtils.AFFILATE_CAMPAIGN_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("DashboardFragment", "onResponse: " + response);
                        if (CommonUtils.hasData(response)) {
                            try {
                                campaigns.addAll(getCampaignListFromJSON(response));
                                loadingScreenLayout.setVisibility(View.GONE);
                                noDataLayout.setVisibility(View.GONE);
                                campaignRv.setVisibility(View.VISIBLE);
                            } catch (JSONException e) {
                                campaignRv.setVisibility(View.GONE);
                                loadingScreenLayout.setVisibility(View.GONE);
                                noDataLayout.setVisibility(View.VISIBLE);
                            }
                        } else {
                            campaignRv.setVisibility(View.GONE);
                            loadingScreenLayout.setVisibility(View.GONE);
                            noDataLayout.setVisibility(View.VISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        campaignRv.setVisibility(View.GONE);
                        loadingScreenLayout.setVisibility(View.GONE);
                        noDataLayout.setVisibility(View.VISIBLE);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerSys = super.getHeaders();
                Map<String, String> headers = new HashMap<>();
                headers.putAll(headerSys);

                // Second parameter is fetching API key from preference
                headers.put("X-Api-Key", PrefsUtils.getUserApiKey(getContext()));
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };
        queue.add(jsonObjectRequest);

        return rootView;
    }

    @Override
    public void onCampaignClick(View view, int position) {
        String campaignId = campaigns.get(position).getId();
        Intent intent = new Intent(getContext(), CampaignDetailActivity.class);
        intent.putExtra("campaignID", campaignId);
        startActivity(intent);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Campaigns");
    }


    /**
     * This function returns the list of Campaign.
     *
     * @param response
     * @return
     */
    private ArrayList<Campaign> getCampaignListFromJSON(JSONObject response) throws JSONException {
        ArrayList<Campaign> campaignList = new ArrayList<>();
        JSONObject dataObject = response.getJSONObject("data");
        JSONArray campaignArray = dataObject.optJSONArray("campaigns");
        for (int i = 0; i < campaignArray.length(); i++) {
            JSONObject campaignObject = campaignArray.getJSONObject(i);
            String title = campaignObject.optString("title");
            String image = campaignObject.optString("image");
            String id = campaignObject.optString("_id");
            ArrayList<String> category = new ArrayList<>();
            for (int j = 0; j < campaignObject.optJSONArray("category").length(); j++) {
                category.add(campaignObject.optJSONArray("category").getString(j));
            }

            String rateStr = "";
            JSONArray commissionArray = campaignObject.optJSONArray("commissions");
            for (int j = 0; j < commissionArray.length(); j++) {
                JSONObject commisionObject = commissionArray.getJSONObject(j);
                String rate = commisionObject.optString("rate");
                String coverageText = "";
                JSONArray coverageArray = commisionObject.optJSONArray("coverage");
                for (int k = 0; k < coverageArray.length(); k++) {
                    coverageText += coverageArray.getString(k);
                    if (k != coverageArray.length() - 1) {
                        coverageText += "/";
                    }
                }
//                rateStr += coverageText + " - $" + CommonUtils.convertPrice(rate);
                rateStr += coverageText + " - " + rate;

                if (j != commissionArray.length() - 1) {
                    rateStr += "\n";
                }
            }

            Campaign campaign = new Campaign(id, title, category, image, rateStr);
            campaignList.add(campaign);
        }
        return campaignList;

    }

}
