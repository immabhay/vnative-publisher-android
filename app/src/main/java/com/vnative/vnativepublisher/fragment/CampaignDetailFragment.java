package com.vnative.vnativepublisher.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.vnative.vnativepublisher.R;
import com.vnative.vnativepublisher.activity.CampaignDetailActivity;
import com.vnative.vnativepublisher.utils.CommonUtils;
import com.vnative.vnativepublisher.utils.HttpsTrustManager;
import com.vnative.vnativepublisher.utils.PrefsUtils;
import com.vnative.vnativepublisher.utils.UrlUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class CampaignDetailFragment extends Fragment implements View.OnClickListener {

    private String campaignID;

    private ImageView campaignImageView;
    private TextView campaignTitleView;
    private TextView statusView;
    private TextView createdView;
    private TextView categoryView;

    private LinearLayout categoryLayout;
    private CardView commisionView;
    private TableLayout commisionTableLayout;
    private CardView linksView;
    private TextView previewUrlView;
    private ImageView urlBrowseView;
    private LinearLayout loadingScreenLayout;
    private TextView noData;
    private LinearLayout campaignLayout;

    private LinearLayout trackingLinkLayout;
    private TextView trackingLinkStatus;
    private TextView trackingLink;
    private ImageView trackingLinkCopy;
    private Button requestPermissionBtn;
    private TextView requestPermissionTv;
    private Button generateLinkBtn;

    RequestQueue queue;

    ClipboardManager clipboardManager;
    ClipData clipData;


    public CampaignDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        campaignID = getActivity().getIntent().getExtras().getString("campaignID");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View campaignView = inflater.inflate(R.layout.fragment_detail_campaign, container, false);

        campaignImageView = (ImageView) campaignView.findViewById(R.id.campaign_iv);
        campaignTitleView = (TextView) campaignView.findViewById(R.id.campaign_title_tv);
        statusView = (TextView) campaignView.findViewById(R.id.status_tv);
        createdView = (TextView) campaignView.findViewById(R.id.date_time_tv);
        categoryView = (TextView) campaignView.findViewById(R.id.category_tv);

        categoryLayout = (LinearLayout) campaignView.findViewById(R.id.category_layout);
        commisionView = (CardView) campaignView.findViewById(R.id.commision_cv);
        commisionTableLayout = (TableLayout) campaignView.findViewById(R.id.commision_tl);
        linksView = (CardView) campaignView.findViewById(R.id.links_cv);
        previewUrlView = (TextView) campaignView.findViewById(R.id.preview_url_tv);
        urlBrowseView = (ImageView) campaignView.findViewById(R.id.url_browse_iv);
        loadingScreenLayout = (LinearLayout) campaignView.findViewById(R.id.loading_screen);
        noData = (TextView) campaignView.findViewById(R.id.no_data);
        campaignLayout = (LinearLayout) campaignView.findViewById(R.id.campaign_layout);

        trackingLinkLayout = (LinearLayout) campaignView.findViewById(R.id.tracking_link_layout);
        trackingLinkStatus = (TextView) campaignView.findViewById(R.id.tracking_link_status_tv);
        trackingLink = (TextView) campaignView.findViewById(R.id.tracking_link_tv);
        trackingLinkCopy = (ImageView) campaignView.findViewById(R.id.tracking_link_copy_iv);
        requestPermissionBtn = (Button) campaignView.findViewById(R.id.btn_request_permission);
        requestPermissionTv = (TextView) campaignView.findViewById(R.id.permission_request_tv);
        generateLinkBtn = (Button) campaignView.findViewById(R.id.btn_generate_link);

        urlBrowseView.setOnClickListener(this);
        requestPermissionBtn.setOnClickListener(this);
        generateLinkBtn.setOnClickListener(this);
        trackingLinkCopy.setOnClickListener(this);

        clipboardManager = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);

        queue = Volley.newRequestQueue(getContext());
        HttpsTrustManager.allowAllSSL();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                UrlUtils.AFFILATE_CAMPAIGN_URL + campaignID, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (CommonUtils.hasData(response)) {
                            try {
                                loadingScreenLayout.setVisibility(View.GONE);
                                noData.setVisibility(View.GONE);
                                campaignLayout.setVisibility(View.VISIBLE);
                                displayData(response);
                            } catch (JSONException e) {
                                loadingScreenLayout.setVisibility(View.GONE);
                                campaignLayout.setVisibility(View.GONE);
                                noData.setVisibility(View.VISIBLE);
                            }
                        } else {
                            loadingScreenLayout.setVisibility(View.GONE);
                            campaignLayout.setVisibility(View.GONE);
                            noData.setVisibility(View.VISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadingScreenLayout.setVisibility(View.GONE);
                        campaignLayout.setVisibility(View.GONE);
                        noData.setVisibility(View.VISIBLE);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerSys = super.getHeaders();
                Map<String, String> headers = new HashMap<>();
                headers.putAll(headerSys);

                // Second parameter is fetching API key from preference
                headers.put("X-Api-Key", PrefsUtils.getUserApiKey(getContext()));
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };
        queue.add(jsonObjectRequest);

        return campaignView;
    }

    private void displayData(JSONObject response) throws JSONException {
        JSONObject dataObject = response.optJSONObject("data");

        JSONObject campaignObject = dataObject.optJSONObject("campaign");

        String title = campaignObject.optString("title");
        campaignTitleView.setText(title);
        ((CampaignDetailActivity) getActivity()).setActionBarTitle(title);

        String previewUrl = campaignObject.optString("preview_url");
        previewUrlView.setText(previewUrl);

        String image = campaignObject.optString("image");
        if (image != "" || image != null) {
            try {
                campaignImageView.setVisibility(View.VISIBLE);
                Picasso.with(getContext()).load(UrlUtils.VNATIVE_IMAGE_BASE_URL +
                        image).into(campaignImageView);
            } catch (Exception e) {
                campaignImageView.setVisibility(View.GONE);
            }
        }

        ArrayList<String> category = new ArrayList<>();
        String categoryText = "";
        for (int i = 0; i < campaignObject.optJSONArray("category").length(); i++) {
            category.add(campaignObject.optJSONArray("category").getString(i));
            categoryText += campaignObject.optJSONArray("category").getString(i);
            if (i != campaignObject.optJSONArray("category").length() - 1) {
                categoryText += ", ";
            }
        }
        categoryView.setText(categoryText);

        Boolean status = campaignObject.optBoolean("live");
        if (status) {
            statusView.setText("Active");
            statusView.setTextColor(Color.WHITE);
            statusView.setBackgroundResource(R.color.active);
        } else {
            statusView.setText("Inactive");
            statusView.setTextColor(Color.WHITE);
            statusView.setBackgroundResource(R.color.inactive);
        }

        String created = campaignObject.optString("created");
        createdView.setText(CommonUtils.getDateTimeString(created));

        JSONArray commissionsArray = dataObject.optJSONArray("commissions");
        for (int i = 0; i < commissionsArray.length(); i++) {
            JSONObject commissionObject = commissionsArray.getJSONObject(i);
            TableRow row = new TableRow(getContext());

            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(0,
                    TableRow.LayoutParams.MATCH_PARENT, 0.33f);

            TextView modelTv = new TextView(getContext());
            modelTv.setGravity(View.TEXT_ALIGNMENT_CENTER);
            modelTv.setText(commissionObject.optString("model").toUpperCase());
            modelTv.setGravity(Gravity.CENTER);
            modelTv.setLayoutParams(layoutParams);


            TextView coverageTv = new TextView(getContext());
            String coverageText = "";
            JSONArray coverageArray = commissionObject.optJSONArray("coverage");
            for (int j = 0; j < coverageArray.length(); j++) {
                coverageText += coverageArray.getString(j);
                if (j != coverageArray.length() - 1) {
                    coverageText += "/";
                }
            }
            coverageTv.setText(coverageText);
            coverageTv.setGravity(Gravity.CENTER);
            coverageTv.setLayoutParams(layoutParams);

            TextView rateTv = new TextView(getContext());
            Double rate = commissionObject.optDouble("rate");
            if (rate.isNaN() || rate == null) {
                rate = 0.0;
            }
            rateTv.setText("$" + CommonUtils.convertPrice(rate));
            rateTv.setGravity(Gravity.CENTER);
            rateTv.setLayoutParams(layoutParams);

            row.addView(modelTv);
            row.addView(coverageTv);
            row.addView(rateTv);

            commisionTableLayout.addView(row);
        }

        // If tracking_link key not found return
        if (!dataObject.has("tracking_link")) {
            return;
        }

        JSONObject trackingLinkObject = dataObject.optJSONObject("tracking_link");
        if (!trackingLinkObject.isNull("url")) {
            String url = trackingLinkObject.optString("url");
            Boolean urlStatus = trackingLinkObject.optBoolean("live");
            displayTrackingLink(url, urlStatus);
        } else {
            JSONObject permission = dataObject.optJSONObject("permission");
            if (permission.optBoolean("permRequired")) {
                JSONObject access = permission.optJSONObject("access");
                if (!access.isNull("_id")) {
                    if (access.optBoolean("live")) {
                        showGenerateLinkButton();
                    } else {
                        showRequestPermissionInfo();
                    }
                } else {
                    showRequestPermissionButton();
                }

            } else {
                showGenerateLinkButton();
            }
        }
    }

    private void displayTrackingLink(String trackingLinkUrl, Boolean trackingLinkStatusVal) {
        if (trackingLinkStatusVal) {
            trackingLinkStatus.setBackgroundResource(R.color.active);
        } else {
            trackingLinkStatus.setBackgroundResource(R.color.inactive);
        }
        trackingLinkStatus.setTextColor(Color.WHITE);
        trackingLink.setText(trackingLinkUrl);
        trackingLinkStatus.setText(CommonUtils.getStatus(trackingLinkStatusVal));
        clipData = ClipData.newPlainText("First Button Clip", trackingLinkUrl);
        showTrackingLinkLayout();
    }

    @Override
    public void onClick(View v) {
        int clickedViewId = v.getId();
        switch (clickedViewId) {
            case R.id.url_browse_iv:
                openPreviewUrl();
                break;
            case R.id.tracking_link_copy_iv:
                copyTrackingLinkUrl();
                break;
            case R.id.btn_request_permission:
                requestPermission();
                break;
            case R.id.btn_generate_link:
                generateLink();
                break;
        }
    }

    private void requestPermission() {
        JsonObjectRequest permissionRequest = new JsonObjectRequest(Request.Method.GET,
                UrlUtils.AFFILATE_REQUEST_ACCESS + campaignID, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.optBoolean("success")) {
                            Toast.makeText(getContext(), "Permission request successful",
                                    Toast.LENGTH_SHORT).show();
                            showRequestPermissionInfo();
                        } else {
                            Toast.makeText(getContext(), "Permission request unsuccessful",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Error requesting permission, Try again!",
                                Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerSys = super.getHeaders();
                Map<String, String> headers = new HashMap<>();
                headers.putAll(headerSys);

                // Second parameter is fetching API key from preference
                headers.put("X-Api-Key", PrefsUtils.getUserApiKey(getContext()));
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };
        queue.add(permissionRequest);
    }

    private void generateLink() {
        JsonObjectRequest linkRequest = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.AFFILATE_LINKS_URL + campaignID, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (response.optBoolean("success")) {
                            JSONObject data = response.optJSONObject("data");
                            JSONObject tracking_link = data.optJSONObject("tracking_link");
                            String url = tracking_link.optString("url");
                            Boolean live = tracking_link.optBoolean("live");
                            displayTrackingLink(url, live);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Error generating link!!",
                                Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerSys = super.getHeaders();
                Map<String, String> headers = new HashMap<>();
                headers.putAll(headerSys);

                // Second parameter is fetching API key from preference
                headers.put("X-Api-Key", PrefsUtils.getUserApiKey(getContext()));
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };
        queue.add(linkRequest);
    }

    /**
     * This function copy tracking link url to clipboard
     */
    public void copyTrackingLinkUrl() {
        clipboardManager.setPrimaryClip(clipData);
        Toast.makeText(getContext(), "Url copied", Toast.LENGTH_SHORT).show();
    }

    /**
     * This function get preview url string from previewUrlView,
     * then start action_view intent
     */
    private void openPreviewUrl() {
        String previewUrl = previewUrlView.getText().toString();
        Uri link = Uri.parse(previewUrl);
        Intent intent = new Intent(Intent.ACTION_VIEW, link);
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    /**
     * This function show generate link text and hide all other views
     */
    private void showRequestPermissionInfo() {
        requestPermissionBtn.setVisibility(View.GONE);
        trackingLinkLayout.setVisibility(View.GONE);
        generateLinkBtn.setVisibility(View.GONE);
        requestPermissionTv.setVisibility(View.VISIBLE);
    }

    /**
     * This function show generate link button and hide all other views
     */
    private void showGenerateLinkButton() {
        requestPermissionBtn.setVisibility(View.GONE);
        trackingLinkLayout.setVisibility(View.GONE);
        requestPermissionTv.setVisibility(View.GONE);
        generateLinkBtn.setVisibility(View.VISIBLE);
    }

    /**
     * This function display request permission button and hide other views
     */
    private void showRequestPermissionButton() {
        trackingLinkLayout.setVisibility(View.GONE);
        requestPermissionTv.setVisibility(View.GONE);
        generateLinkBtn.setVisibility(View.GONE);
        requestPermissionBtn.setVisibility(View.VISIBLE);
    }

    /**
     * This function display tracking link layout and hide other views
     */
    private void showTrackingLinkLayout() {
        requestPermissionTv.setVisibility(View.GONE);
        generateLinkBtn.setVisibility(View.GONE);
        requestPermissionBtn.setVisibility(View.GONE);
        trackingLinkLayout.setVisibility(View.VISIBLE);
    }

}
