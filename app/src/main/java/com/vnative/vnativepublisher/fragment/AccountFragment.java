package com.vnative.vnativepublisher.fragment;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vnative.vnativepublisher.R;
import com.vnative.vnativepublisher.utils.CommonUtils;
import com.vnative.vnativepublisher.utils.HttpsTrustManager;
import com.vnative.vnativepublisher.utils.PrefsUtils;
import com.vnative.vnativepublisher.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class AccountFragment extends Fragment implements View.OnClickListener {

    private TextView accountNameTv;
    private TextView accountUsernameTv;
    private TextView accountEmailTv;
    private TextView accountPhoneTv;
    private TextView accountNameLoadingTv;
    private TextView accountUsernameLoadingTv;
    private TextView accountEmailLoadingTv;
    private TextView accountPhoneLoadingTv;

    private LinearLayout accountDetailLayout;
    private LinearLayout accountEditLayout;
    private CardView accountErrorCard;

    private EditText accountNameEt;
    private EditText accountUsernameEt;
    private EditText accountPhoneEt;

    private Button editAccountBtn;
    private Button updateAccountBtn;
    private Button cancelBtn;

    private ScrollView fragmentAccountScrollView;

    RequestQueue queue;

    public AccountFragment() {
        // required
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_account, container, false);
        accountNameTv = (TextView) rootView.findViewById(R.id.account_name);
        accountUsernameTv = (TextView) rootView.findViewById(R.id.account_username);
        accountEmailTv = (TextView) rootView.findViewById(R.id.account_email);
        accountPhoneTv = (TextView) rootView.findViewById(R.id.account_phone);
        accountNameLoadingTv = (TextView) rootView.findViewById(R.id.account_name_loading_tv);
        accountUsernameLoadingTv = (TextView) rootView.findViewById(R.id.account_username_loading_tv);
        accountEmailLoadingTv = (TextView) rootView.findViewById(R.id.account_email_loading_tv);
        accountPhoneLoadingTv = (TextView) rootView.findViewById(R.id.account_phone_loading_tv);

        accountDetailLayout = (LinearLayout) rootView.findViewById(R.id.show_account_detail_layout);
        accountEditLayout = (LinearLayout) rootView.findViewById(R.id.show_account_edit_layout);
        accountErrorCard = (CardView) rootView.findViewById(R.id.account_error_card);

        accountNameEt = (EditText) rootView.findViewById(R.id.account_name_et);
        accountUsernameEt = (EditText) rootView.findViewById(R.id.account_username_et);
        accountPhoneEt = (EditText) rootView.findViewById(R.id.account_phone_et);

        editAccountBtn = (Button) rootView.findViewById(R.id.edit_account_btn);
        editAccountBtn.setOnClickListener(this);
        updateAccountBtn = (Button) rootView.findViewById(R.id.update_account_btn);
        updateAccountBtn.setOnClickListener(this);
        cancelBtn = (Button) rootView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(this);

        fragmentAccountScrollView = (ScrollView) rootView.findViewById(R.id.fragment_account);

        queue = Volley.newRequestQueue(getContext());
        requestData();
        return rootView;
    }

    private void requestData() {
        HttpsTrustManager.allowAllSSL();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                UrlUtils.AFFILATE_ACCOUNT_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (CommonUtils.hasData(response)) {
                            displayData(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        displayAccountErrorLoadMessage();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerSys = super.getHeaders();
                Map<String, String> headers = new HashMap<>();
                headers.putAll(headerSys);
                headers.put("X-Api-Key", PrefsUtils.getUserApiKey(getContext()));
                headers.put("Cache-Control", "no-cache");
                return headers;
            }
        };
        queue.add(jsonObjectRequest);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Account");
    }

    @Override
    public void onClick(View v) {
        int clickedItemId = v.getId();
        switch (clickedItemId) {
            case R.id.edit_account_btn:
                showAccountEditLayout();
                break;
            case R.id.update_account_btn:
                updateAccountInfo();
                break;
            case R.id.cancel_btn:
                showAccountDetailLayout();
                break;
        }
    }

    private void showAccountEditLayout() {
        accountDetailLayout.setVisibility(View.GONE);
        accountEditLayout.setVisibility(View.VISIBLE);
    }

    private void showAccountDetailLayout() {
        accountEditLayout.setVisibility(View.GONE);
        accountDetailLayout.setVisibility(View.VISIBLE);
    }

    private void updateAccountInfo() {
        String name = accountNameEt.getText().toString().trim();
        String username = accountUsernameEt.getText().toString().trim();
        String phone = accountPhoneEt.getText().toString().trim();
        if (!name.isEmpty() && !username.isEmpty() && !phone.isEmpty()) {
            JSONObject data = new JSONObject();
            try {
                data.put("action", "basic_info");
                data.put("name", name);
                data.put("username", username);
                data.put("phone", phone);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest accountRequest = new JsonObjectRequest(Request.Method.PATCH,
                    UrlUtils.AFFILATE_ACCOUNT_URL, data,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (CommonUtils.hasData(response)) {
                                Snackbar snackbar = Snackbar.make(fragmentAccountScrollView,
                                        "Account info updated!!", Snackbar.LENGTH_SHORT);
                                snackbar.show();
                                showAccountDetailLayout();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Snackbar snackbar = Snackbar.make(fragmentAccountScrollView,
                                    "Error : Updating account info !!", Snackbar.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headerSys = super.getHeaders();
                    Map<String, String> headers = new HashMap<>();
                    headers.putAll(headerSys);
                    headers.put("X-Api-Key", PrefsUtils.getUserApiKey(getContext()));
                    headers.put("Cache-Control", "no-cache");
                    return headers;
                }
            };
            queue.add(accountRequest);
            requestData();
            showAccountDetailLayout();
        } else {
            Snackbar snackbar = Snackbar.make(fragmentAccountScrollView,
                    "All fields are required!!", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    private void displayData(JSONObject response) {
        JSONObject dataObject = response.optJSONObject("data");
        JSONObject userObject = dataObject.optJSONObject("user");
        String username = userObject.optString("username");
        String name = userObject.optString("name");
        String email = userObject.optString("email");
        String phone = userObject.optString("phone");
        if (phone.isEmpty() || phone == null) {
            accountPhoneTv.setText("Not available");
            accountPhoneEt.setHint("Enter phone number");
        } else {
            accountPhoneTv.setText(phone);
            accountPhoneEt.setText(phone);
        }

        hideAccountDataLoadingViews();
        displayAccountDataViews();
        accountNameTv.setText(name);
        accountUsernameTv.setText(username);
        accountEmailTv.setText(email);

        accountNameEt.setText(name);
        accountUsernameEt.setText(username);
        accountPhoneEt.setText(phone);
    }

    /**
     * This function display all the data text views
     */
    private void displayAccountDataViews() {
        accountNameTv.setVisibility(View.VISIBLE);
        accountUsernameTv.setVisibility(View.VISIBLE);
        accountEmailTv.setVisibility(View.VISIBLE);
        accountPhoneTv.setVisibility(View.VISIBLE);
    }

    /**
     * This function display all the data loading text views
     */
    private void hideAccountDataLoadingViews() {
        accountNameLoadingTv.setVisibility(View.INVISIBLE);
        accountUsernameLoadingTv.setVisibility(View.INVISIBLE);
        accountEmailLoadingTv.setVisibility(View.INVISIBLE);
        accountPhoneLoadingTv.setVisibility(View.INVISIBLE);
    }

    /**
     * This function will display error message if data is not loaded correctly.
     */
    private void displayAccountErrorLoadMessage() {
        accountDetailLayout.setVisibility(View.INVISIBLE);
        accountErrorCard.setVisibility(View.VISIBLE);
    }

}
